import React, { Component } from 'react';
import axios from 'axios';
import {ToastsContainer, ToastsStore} from 'react-toasts';

export default class DocsList extends Component {

    constructor(props) {
        super(props);        
        this.state = {docs: []};
    }

    componentDidMount() {
        axios.get('http://localhost:4000/docs/')
            .then(response => {
                this.setState({ docs: response.data });
            })
    }

    edit(id) {
        this.props.history.push('/edit/' + id)
    }

    remove(id) {
        axios.post('http://localhost:4000/docs/remove/' + id)
            .then(response => {
                let docs = this.state.docs;
                for(let i = 0; i < docs.length; i++){
                    if(docs[i]._id === id) {
                        docs.splice(i, 1);
                        this.setState({ docs: docs });
                        ToastsStore.success(response.data)
                        return;
                    }
                }
            }).catch(function (error){
                ToastsStore.error("Error!")
            })
    }

    render() {
        return (
            <div>
                <h3 align="center" className="mb-2">Documentos</h3>
                <div className="row mt-4">
                    { this.state.docs.map((doc, i) =>
                        <div key={i} className="col-sm-6 mb-4 card-deck">
                            <div className="card">
                                <div className="card-header">{doc.doc_title}</div>
                                <div className="card-body ml-2">
                                    <div className="row">
                                        <span><strong>Claves: </strong></span>
                                        <span className="ml-1">
                                            {doc.doc_keys.map((item, j) =>
                                                <span className="mr-1 chip" key={j}>
                                                    {item}
                                                </span>
                                            )}
                                        </span>
                                    </div>
                                    <div className="row">
                                        <span><strong>Descripción: </strong></span>
                                        <span className="ml-1">
                                            {doc.doc_description}
                                        </span>
                                    </div>
                                    <div className="row">
                                        <span><strong>Fuente: </strong></span>
                                        <span className="ml-1">
                                            {doc.doc_source}
                                        </span>
                                    </div>
                                    <div className="row">
                                        <span><strong>Tipo de Recurso: </strong></span>
                                        <span className="ml-1">
                                            {doc.doc_type}
                                        </span>
                                    </div>
                                    <div className="row">
                                        <span><strong>Cobertura: </strong></span>
                                        <span className="ml-1 chip">
                                            {doc.doc_frame.from}
                                        </span>
                                        <span className="ml-1 chip">
                                            {doc.doc_frame.to}
                                        </span>
                                        <span className="ml-1 chip">
                                            {doc.doc_frame.local}
                                        </span>
                                    </div>
                                </div>
                                <div className="card-footer text-center">
                                    <div className="row">
                                        <div className="col">
                                            <button 
                                                className="btn btn-warning btn-block" 
                                                onClick={() => this.edit(doc._id)}
                                            >
                                                Editar
                                            </button>
                                        </div>
                                        <div className="col">
                                            <button 
                                                className="btn btn-danger btn-block"
                                                onClick={() => this.remove(doc._id)}
                                            >
                                                Eliminar
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )}
                </div>

                <ToastsContainer store={ToastsStore}/>
            </div>
        )
    }
}