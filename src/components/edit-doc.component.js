import React, { Component } from 'react';
import axios from 'axios';
import {ToastsContainer, ToastsStore} from 'react-toasts';

export default class EditDoc extends Component {

    constructor(props) {
        super(props);

        this.onChangeHelperNewKey = this.onChangeHelperNewKey.bind(this);
        this.onChangeHelperDateFrom = this.onChangeHelperDateFrom.bind(this);
        this.onChangeHelperDateTo = this.onChangeHelperDateTo.bind(this);
        this.onChangeHelperLocal = this.onChangeHelperLocal.bind(this);
        
        this.onChangeDocTitle = this.onChangeDocTitle.bind(this);
        this.onAddDocKey = this.onAddDocKey.bind(this);
        this.onRemoveLastDocKey = this.onRemoveLastDocKey.bind(this);
        this.onChangeDocDescription = this.onChangeDocDescription.bind(this);
        this.onChangeDocSource = this.onChangeDocSource.bind(this);
        this.onChangeDocType = this.onChangeDocType.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            helper_new_key: '',
            helper_date_from: '',
            helper_date_to: '',
            helper_local: '',
            doc_title: '',
            doc_keys: [],
            doc_description: '',
            doc_source: '',
            doc_type: '',
            doc_frame: {
                from: '',
                to: '',
                local: ''
            }
        }
    }

    componentDidMount() {
        axios.get('http://localhost:4000/docs/' + this.props.match.params.id)
            .then(response => {
                this.setState({
                    helper_date_from: response.data.doc_frame.from,
                    helper_date_to: response.data.doc_frame.to,
                    helper_local: response.data.doc_frame.local,

                    doc_title: response.data.doc_title,
                    doc_keys: response.data.doc_keys,
                    doc_description: response.data.doc_description,
                    doc_source: response.data.doc_source,
                    doc_type: response.data.doc_type,
                    doc_frame: response.data.doc_frame 
                })   
            })
            .catch(function (error) {})
    }

    onChangeHelperNewKey(e) {
        this.setState({
            helper_new_key: e.target.value
        });
    }

    onChangeHelperDateFrom(e) {
        this.setState({
            helper_date_from: e.target.value,
            doc_frame: {
                from: e.target.value,
                to: this.state.helper_date_to,
                local: this.state.helper_local
            }
        });
    }

    onChangeHelperDateTo(e) {
        this.setState({
            helper_date_to: e.target.value,
            doc_frame: {
                from: this.state.helper_date_from,
                to: e.target.value,
                local: this.state.helper_local
            }
        });
    }

    onChangeHelperLocal(e) {
        this.setState({
            helper_local: e.target.value,
            doc_frame: {
                from: this.state.helper_date_from,
                to: this.state.helper_date_to,
                local: e.target.value
            }
        });
    }

    onChangeDocTitle(e) {
        this.setState({
            doc_title: e.target.value
        });
    }

    onAddDocKey(e) {
        if(!this.state.helper_new_key){
            return;
        }
        
        let keys = this.state.doc_keys;
        keys.push(this.state.helper_new_key);

        this.setState({
            helper_new_key: '',
            doc_keys: keys,
        });
    }

    onRemoveLastDocKey(e) {        
        let keys = this.state.doc_keys;
        keys.pop();

        this.setState({
            doc_keys: keys,
        });
    }
    
    onChangeDocDescription(e) {
        this.setState({
            doc_description: e.target.value
        });
    }

    onChangeDocSource(e) {
        this.setState({
            doc_source: e.target.value
        });
    }

    onChangeDocType(e) {
        this.setState({
            doc_type: e.target.value
        });
    }

    onSubmit(e) {
        e.preventDefault();
        
        const obj = {
            doc_title: this.state.doc_title,
            doc_keys: this.state.doc_keys,
            doc_description: this.state.doc_description,
            doc_source: this.state.doc_source,
            doc_type: this.state.doc_type,
            doc_frame: this.state.doc_frame,
        };
        
        axios.post('http://localhost:4000/docs/update/' + this.props.match.params.id, obj)
            .then(res => {
                ToastsStore.success(res.data)
            })
            .catch(function (error){
                ToastsStore.error("Error!")
            })
    }

    render() {
        return (
            <div>
                <h3 align="center">Editar Documento</h3>
                <form onSubmit={this.onSubmit}>
                    
                    <div className="form-group"> 
                        <label>Título: </label>
                        <input  type="text"
                                className="form-control"
                                value={this.state.doc_title}
                                onChange={this.onChangeDocTitle}
                        />
                    </div>
                    <div className="form-group">
                        <label>Claves: </label>
                        <div className="row">
                            <div className="col-6">
                                <input 
                                    type="text" 
                                    className="form-control"
                                    value={this.state.helper_new_key}
                                    onChange={this.onChangeHelperNewKey}
                                />
                            </div>
                            <div className="col-3">
                                <input 
                                    type="button"
                                    value="Agregar" 
                                    className="btn btn-primary btn-block" 
                                    onClick={this.onAddDocKey}
                                />
                            </div>
                            <div className="col-3">
                                <input 
                                    type="button"
                                    value="Eliminar" 
                                    className="btn btn-danger btn-block" 
                                    onClick={this.onRemoveLastDocKey}
                                />
                            </div>
                        </div>                        
                        <div className="row mt-2">
                            <div className="col">
                                {this.state.doc_keys.map(function(item, i) {
                                    return (
                                        <div className="mr-1 chip" key={i}>
                                            {item}
                                        </div>
                                    )
                                })}
                            </div>
                        </div>
                    </div>
                    <div className="form-group"> 
                        <label>Descripción: </label>
                        <input  type="text"
                                className="form-control"
                                value={this.state.doc_description}
                                onChange={this.onChangeDocDescription}
                        />
                    </div>
                    <div className="form-group"> 
                        <label>Fuente: </label>
                        <input  type="text"
                                className="form-control"
                                value={this.state.doc_source}
                                onChange={this.onChangeDocSource}
                        />
                    </div>
                    <div className="form-group">
                        <label>Tipo de recurso:</label>
                        <div></div>
                        <div className="form-check form-check-inline">
                            <input  className="form-check-input" 
                                    type="radio" 
                                    name="priorityOptions"
                                    value="Testimonio"
                                    checked={this.state.doc_type==='Testimonio'} 
                                    onChange={this.onChangeDocType}
                            />
                            <label className="form-check-label">Testimonio</label>
                        </div>
                        <div className="form-check form-check-inline">
                            <input  className="form-check-input" 
                                    type="radio" 
                                    name="priorityOptions"
                                    value="Informe" 
                                    checked={this.state.doc_type==='Informe'} 
                                    onChange={this.onChangeDocType}
                            />
                            <label className="form-check-label">Informe</label>
                        </div>
                        <div className="form-check form-check-inline">
                            <input  className="form-check-input" 
                                    type="radio" 
                                    name="priorityOptions" 
                                    value="Caso" 
                                    checked={this.state.doc_type==='Caso'} 
                                    onChange={this.onChangeDocType}
                            />
                            <label className="form-check-label">Caso</label>
                        </div>
                    </div>
                    <div className="form-group">
                        <label>Cobertura: </label>
                        
                        <div className="row">
                            <div className="col">
                                <input 
                                    type="date" 
                                    className="form-control"
                                    placeholder="Rango Fecha - Desde"
                                    value={this.state.helper_date_from}
                                    onChange={this.onChangeHelperDateFrom}
                                />
                            </div>
                            <div className="col">
                                <input 
                                    type="date" 
                                    className="form-control"
                                    placeholder="Rango Fecha - Hasta"
                                    value={this.state.helper_date_to}
                                    onChange={this.onChangeHelperDateTo}
                                />
                            </div>
                            <div className="col">
                                <input 
                                    type="text" 
                                    className="form-control"
                                    placeholder="Ubicación"
                                    value={this.state.helper_local}
                                    onChange={this.onChangeHelperLocal}
                                />
                            </div>
                        </div>
                    </div>

                    <br/>
                    <div className="form-group">
                    <input 
                        type="submit" 
                        value="Editar" 
                        className="btn btn-primary btn-block"
                        disabled = {
                            this.state.doc_title === '' ||
                            this.state.doc_keys.length <= 0 ||
                            this.state.doc_description === '' ||
                            this.state.doc_source === '' ||
                            this.state.doc_type === '' ||
                            this.state.doc_frame.from === '' ||
                            this.state.doc_frame.to === '' ||
                            this.state.doc_frame.local === ''
                        }
                    />
                    </div>
                </form>

                <ToastsContainer store={ToastsStore}/>
            </div>
        )
    }
}