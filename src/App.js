import React from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import './App.css';
import "bootstrap/dist/css/bootstrap.min.css";

import DocsList from "./components/docs-list.component";
import EditDoc from "./components/edit-doc.component";
import CreateDoc from "./components/create-doc.component";

import logo from './logo.svg';

function App() {
    return (
        <Router>
            <div className="container">
                <nav className="navbar navbar-expand-lg navbar-light bg-light">
                    <a href="/" className="navbar-brand">
                        <img src={logo} width="30" height="30" alt="img" />
                    </a>
                    <div to="/" className="navbar-brand">Documentos Comisión Verdad</div>
                    <div className="collpase navbar-collapse">
                        <ul className="navbar-nav mr-auto">
                            <li className="navbar-item">
                                <Link to="/" className="nav-link">Ver</Link>
                            </li>
                            <li className="navbar-item">
                                <Link to="/create" className="nav-link">Crear</Link>
                            </li>
                        </ul>
                    </div>
                </nav>
                <br/>
                <Route path="/" exact component={DocsList} />
                <Route path="/edit/:id" component={EditDoc} />
                <Route path="/create" component={CreateDoc} />
            </div>
        </Router>
    );
}

export default App;
