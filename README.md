# `cvDocsClient`

## `1. get started`

### `nodejs`

version: 10

download link: [https://nodejs.org/es/download](https://nodejs.org/es/download)


### `extra packages`
```
npm update
```

## `2. run client`
```
npm start
```